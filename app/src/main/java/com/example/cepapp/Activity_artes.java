package com.example.cepapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity_artes extends AppCompatActivity {

    private Button lartesdigitales,
            lcomposicion,
            lmusica,
            tmusica,
            tproducciondigital,
            tmusicaytecnologia,
            lartesytecnologia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artes);
        //botones con direccion
        lartesdigitales = findViewById(R.id.button_lartes);
        lcomposicion = findViewById(R.id.button_comp);
        lmusica = findViewById(R.id.button_lMusica);
        tmusica = findViewById(R.id.button_tmusica);
        tproducciondigital = findViewById(R.id.button_produccion);
        tmusicaytecnologia = findViewById(R.id.button_tmusica);
        lartesytecnologia = findViewById(R.id.button_artes_vir);



        //botones con pagina
        this.pagina(lartesdigitales,"http://www.unq.edu.ar/carreras/81-licenciatura-en-artes-digitales.php");
        this.pagina(lcomposicion,"http://www.unq.edu.ar/carreras/25-licenciatura-en-composici%C3%B3n-con-medios-electroac%C3%BAsticos.php");
        this.pagina(lmusica, "http://www.unq.edu.ar/carreras/67-licenciatura-en-econom%C3%ADa-del-desarrollo.php");
        this.pagina(tmusica, "http://www.unq.edu.ar/carreras/100-tecnicatura-universitaria-en-creaci%C3%B3n-musical.php");
        this.pagina(tproducciondigital, "http://www.unq.edu.ar/carreras/69-tecnicatura-universitaria-en-producci%C3%B3n-digital.php");
        this.pagina(tmusicaytecnologia,"http://www.unq.edu.ar/carreras/101-tecnicatura-universitaria-en-producci%C3%B3n-musical-y-nuevas-tecnolog%C3%ADas.php");
        this.pagina(lartesytecnologia, "http://www.unq.edu.ar/carreras/50-licenciatura-en-artes-y-tecnolog%C3%ADas-ccc.php");




        this.setTitle(R.string.text_Artes);
    }
    public void pagina(Button boton, final String url) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }

        });
    }



}