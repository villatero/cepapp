package com.example.cepapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity_Sociales extends AppCompatActivity {

    private Button lsociales,
    lcomunicacion,
    leducacion,
    leducacionccc,
    lenfermeria,
    lhistoria,
    lterapia,
    psocailes,
    pcomunicacion,
    peducacion,
    phistoria,
    lsocialesvirtual,
    leducacionCCCvirual,
    lgeografiavirtual,
    tgestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__sociales);

        //boton con direccion
        lsociales = findViewById(R.id.button_lsociales);
        lcomunicacion = findViewById(R.id.button_lcomunicacion);
        leducacion = findViewById(R.id.button_ledu);
        leducacionccc = findViewById(R.id.button_leduCCC);
        lenfermeria = findViewById(R.id.button_lEnfermeria);
        lhistoria = findViewById(R.id.button_lhist);
        lterapia = findViewById(R.id.button_tera);
        psocailes = findViewById(R.id.button_psociales);
        pcomunicacion = findViewById( (R.id.button_pcom));
        peducacion = findViewById(R.id.button_peducacion);
        phistoria = findViewById(R.id.button_phistoria);
        lsocialesvirtual = findViewById(R.id.button_lsociales_vir);
        leducacionCCCvirual = findViewById(R.id.button_leducacion_virtual);
        lgeografiavirtual = findViewById(R.id.button_lgeografia);
        tgestion = findViewById(R.id.button_tgestion);

        //boton con paginathis.pagina(lcomunicacion,
        this.pagina(lsociales,"http://www.unq.edu.ar/carreras/22-licenciatura-en-ciencias-sociales.php");
       this.pagina(lcomunicacion,"http://www.unq.edu.ar/carreras/24-licenciatura-en-comunicaci%C3%B3n-social.php");
       this.pagina(leducacion,"http://www.unq.edu.ar/carreras/26-licenciatura-en-educaci%C3%B3n.php");
       this.pagina(leducacionccc,"http://www.unq.edu.ar/carreras/83-licenciatura-en-educaci%C3%B3n-ccc.php");
       this.pagina(lenfermeria,"http://www.unq.edu.ar/carreras/47-licenciatura-en-enfermer%C3%ADa.php");
       this.pagina(lhistoria,"http://www.unq.edu.ar/carreras/55-licenciatura-en-historia.php");
       this.pagina(lterapia,"http://www.unq.edu.ar/carreras/28-licenciatura-en-terapia-ocupacional.php");
       this.pagina(psocailes,"http://www.unq.edu.ar/carreras/29-profesorado-de-ciencias-sociales.php");
       this.pagina(pcomunicacion,"http://www.unq.edu.ar/carreras/30-profesorado-de-comunicaci%C3%B3n-social.php");
       this.pagina(peducacion,"http://www.unq.edu.ar/carreras/31-profesorado-de-educaci%C3%B3n.php");
       this.pagina(phistoria,"http://www.unq.edu.ar/carreras/95-profesorado-de-historia.php");
       this.pagina(lsocialesvirtual,"http://www.unq.edu.ar/carreras/45-licenciatura-en-ciencias-sociales-y-humanidades-ccc.php");
       this.pagina(leducacionCCCvirual,"http://www.unq.edu.ar/carreras/46-licenciatura-en-educaci%C3%B3n-ccc.php");
       this.pagina(lgeografiavirtual,"http://www.unq.edu.ar/carreras/89-licenciatura-en-geograf%C3%ADa-ccc.php");
       this.pagina(tgestion,"http://www.unq.edu.ar/carreras/44-tecnicatura-en-gesti%C3%B3n-de-medios-comunitarios.php");



        this.setTitle(R.string.text_Sociales);
    }
    public void pagina(Button boton, final String url) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }

        });
    }


}