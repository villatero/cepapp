package com.example.cepapp;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_contactos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_contactos extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Fragment_contactos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment_contactos.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_contactos newInstance(String param1, String param2) {
        Fragment_contactos fragment = new Fragment_contactos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private ImageButton facebook, instagram, whatsapp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_contactos, container, false);


        //botones con sus direcciones
        facebook = view.findViewById(R.id.imageButton_facebook);
        instagram = view.findViewById(R.id.imageButton_instagram);
        whatsapp = view.findViewById(R.id.imageButton_whatsapp);

        this.paginas(facebook, "https://m.facebook.com/cepa.unq");
        this.paginas(instagram,"https://www.instagram.com/cepa_unq/?hl=es-la");

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbrirWhatsApp( "1123991152");
            }
        });



       return view;
    }

    public void paginas(ImageButton boton, final String url) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }

        });
    }

    private void AbrirWhatsApp(String telefono)
    {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
        intent.putExtra("jid", PhoneNumberUtils.stripSeparators("+54 9" + telefono)+"@s.whatsapp.net");
        startActivity(intent);
    }
}