package com.example.cepapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity_Ecnomia extends AppCompatActivity {
    private Button lhoteleria,
            lcomercio,
            leconomia,
            lrecursos,
            tconomia,
            tpymes,
            contador,
            ladminitracion,
            lcomerciovirtual,
            lturismo,
            tempresariales;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__economia);
        //botones con su direccion
        lhoteleria = findViewById(R.id.button_lhotel);
        lcomercio = findViewById(R.id.button_comer);
        leconomia = findViewById(R.id.button_leconomia);
        lrecursos = findViewById(R.id.button_lgestion);
        tconomia = findViewById(R.id.button_teconomia);
        tpymes = findViewById(R.id.button_tpymes);
        contador = findViewById(R.id.button_contador_vir);
        ladminitracion = findViewById(R.id.button_admi_vir);
        lcomerciovirtual = findViewById(R.id.button_comercio_vir);
        lturismo = findViewById(R.id.button_turismo);
        tempresariales = findViewById(R.id.button_tEmpresa_vir);

        this.setTitle(R.string.text_Economia);

        //botones con su pagina
        this.pagina(lhoteleria,"http://www.unq.edu.ar/carreras/20-licenciatura-en-administraci%C3%B3n-hotelera.php");
        this.pagina(lcomercio,"http://www.unq.edu.ar/carreras/23-licenciatura-en-comercio-internacional.php");
        this.pagina(leconomia,"http://www.unq.edu.ar/carreras/67-licenciatura-en-econom%C3%ADa-del-desarrollo.php");
        this.pagina(lrecursos,"http://www.unq.edu.ar/carreras/78-licenciatura-en-gesti%C3%B3n-de-recursos-humanos-y-relaciones-laborales.php");
        this.pagina(tconomia,"http://www.unq.edu.ar/carreras/41-tecnicatura-universitaria-en-econom%C3%ADa-social-y-solidaria.php");
        this.pagina(tpymes,"http://www.unq.edu.ar/carreras/87-tecnicatura-universitaria-en-gesti%C3%B3n-de-peque%C3%B1as-y-medianas-empresas.php");
        this.pagina(contador,"http://www.unq.edu.ar/carreras/43-contador-p%C3%BAblico-nacional.php");
        this.pagina(ladminitracion,"http://www.unq.edu.ar/carreras/35-licenciatura-en-administraci%C3%B3n.php");
        this.pagina(lcomerciovirtual, "http://www.unq.edu.ar/carreras/48-licenciatura-en-comercio-internacional.php");
        this.pagina(lturismo,"http://www.unq.edu.ar/carreras/36-licenciatura-en-turismo-y-hoteler%C3%ADa.php");
        this.pagina(tempresariales,"http://www.unq.edu.ar/carreras/42-tecnicatura-universitaria-en-ciencias-empresariales.php");




    }
    public void pagina(Button boton, final String url) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }

        });
    }
}