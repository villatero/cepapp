package com.example.cepapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

public class Fragment_coeficiente extends Fragment {

    private EditText promedio, creditoop, creditosT;
    private TextView resultado;
    private Button calcula;
    private ImageButton pro,creo,cret;;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_coeficiente, container, false);

        promedio = (EditText) view.findViewById(R.id.editTex_promedio);
        creditoop = (EditText) view.findViewById(R.id.editText_creditos_optenidos);
        creditosT = (EditText) view.findViewById(R.id.editText_creditos_totales);

        resultado = (TextView) view.findViewById(R.id.textView_resultado);

        calcula = (Button) view.findViewById(R.id.button_calcular);
        calcula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calularPromedio(view);
            }
        });

        //informacion de cada celda
        pro = (ImageButton) view.findViewById(R.id.imageButton_promedio);
        pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info("Sacar promedio","Se saca sumando todas tus notas (aprobadas y desaprobadas) y dividiendo por la cantidad de materias que cursaste (no se cuenta los ausentes)");
            }
        });

        creo = (ImageButton) view.findViewById(R.id.imageButton_creo);
        creo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info("Creditos obtenidos","Es la suma de todos los creditos de las materias aprobadas");
            }
        });

        cret = (ImageButton) view.findViewById(R.id.imageButton_cret);
        cret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info("Creditos totales","Es el credito total de la carrera");
            }
        });

        return view;
    }

    public void calularPromedio(View view) {
        //Calcula el promedio
        String pro = promedio.getText().toString();
        String cret = creditosT.getText().toString();
        String creo = creditoop.getText().toString();



        //notificacion de error
        tieneNull(pro, "No pusiste tu promedio");
        tieneNull(cret, "No pusiste los creditos totales");
        tieneNull(creo, "No pusiste los creditos optenido");

        coeficiente(pro, cret, creo);
    }


    public void coeficiente(String pro, String cret, String creo) {
        //calcula el coeficiente y si el cret tiene 0 se corta
        if ((pro.length() != 0) && (cret.length() != 0) && (creo.length() != 0) ) {
            double promedioi = Double.parseDouble(pro);
            double creti = Double.parseDouble(cret);
            double creoi = Double.parseDouble(creo);

            calculcarSincero(promedioi,creoi,creti);
        }
    }


    public void calcular (double pro, double creo, double cret){
        //calculo el coefienciente
            double resul = (pro / 2) + ((creo / cret) * 5);
            String result = String.valueOf(resul);
            resultado.setText(result);
    }


    public void tieneNull(String string, String frase) {
        //Verifica si no completaste alguna celda
        if (string.length() == 0) {
            //Aca esta la advertencia si esta en falta.
            Toast.makeText(getActivity(), frase, Toast.LENGTH_LONG).show();
        }
    }


    public void calculcarSincero(double pro, double creo, double cret){
        //calcula el coficiente si no hay un cero en "Creditos totales"
        if (cret == 0){
            Toast.makeText(getActivity(), "Los creditos totales no pueden valer cero", Toast.LENGTH_LONG).show();
        }else {
            calcular(pro,creo,cret);
        }
    }

    private void info(String titulo, String texto){
        //crea las pantalla flotante de informacion
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(titulo);
        builder.setMessage(texto).show();
    }
    
}
