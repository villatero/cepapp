package com.example.cepapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity_CyT extends AppCompatActivity {

    private Button naval,
            alimento,
            iaci,
            bioinformatica,
            lbiotecnologia,
            tbioitecnologia,
            linformatica,
            tpi,
            tquimica,
            tambiente,
            tseguridad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__cyt);
        //botones con sus direcciones
        naval = findViewById(R.id.button_naval);
        alimento = findViewById(R.id.button_alimento);
        iaci = findViewById(R.id.button_iaci);
        bioinformatica = findViewById(R.id.button_bioinformatica);
        lbiotecnologia = findViewById(R.id.button_lbio);
        tbioitecnologia = findViewById(R.id.button_tbio);
        linformatica = findViewById(R.id.button_inform);
        tpi = findViewById(R.id.button_tpi);
        tquimica = findViewById(R.id.button_tquimica);
        tambiente = findViewById( R.id.button_amb);
        tseguridad  =findViewById(R.id.button_seg);


        this.setTitle(R.string.text_CyT);

        //botones con sus paginas
        this.paginas(naval, "http://www.unq.edu.ar/carreras/17-arquitectura-naval.php");
        this.paginas(iaci,"http://www.unq.edu.ar/carreras/19-ingenier%C3%ADa-en-automatizaci%C3%B3n-y-control-industrial.php");
        this.paginas(alimento, "http://www.unq.edu.ar/carreras/18-ingenier%C3%ADa-en-alimentos.php");
        this.paginas(bioinformatica,"http://www.unq.edu.ar/carreras/73-licenciatura-en-bioinform%C3%A1tica.php");
        this.paginas(lbiotecnologia,"http://www.unq.edu.ar/carreras/21-licenciatura-en-biotecnolog%C3%ADa.php");
        this.paginas(tbioitecnologia, "http://www.unq.edu.ar/carreras/84-tecnicatura-universitaria-en-biotecnolog%C3%ADa.php");
        this.paginas(linformatica, "http://www.unq.edu.ar/carreras/58-licenciatura-en-inform%C3%A1tica.php");
        this.paginas(tpi, "http://www.unq.edu.ar/carreras/32-tecnicatura-universitaria-en-programaci%C3%B3n-inform%C3%A1tica.php");
        this.paginas(tquimica, "http://www.unq.edu.ar/carreras/72-tecnicatura-universitaria-en-qu%C3%ADmica.php");
        this.paginas(tambiente,"http://www.unq.edu.ar/carreras/77-tecnicatura-universitaria-en-tecnolog%C3%ADa-ambiental-y-petroqu%C3%ADmica.php");
        this.paginas(tseguridad,"http://www.unq.edu.ar/carreras/106-tecnicatura-universitaria-en-seguridad-e-higiene-en-el-trabajo.php");
    }
    public void paginas(Button boton, final String url) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }

        });
    }

}