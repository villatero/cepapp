package com.example.cepapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

public class Fragment_carreras extends Fragment {
    Button sociales, CyT, economia, artes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_dimension, container, false);

        View view = inflater.inflate(R.layout.fragment_carreras, container, false);

        sociales = (Button) view.findViewById(R.id.button_Sociales);
        sociales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent Bmeters = new Intent( getActivity() ,Activity_Sociales.class);
               startActivity(Bmeters);
            }
        });

        CyT=(Button)view.findViewById(R.id.button_CyT);
        CyT.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                Intent Bmeters = new Intent(getActivity(), Activity_CyT.class);
                startActivity(Bmeters);
            }
        });
        economia= (Button)view.findViewById(R.id.button_Economia);
        economia.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 Intent Bmeters = new Intent(getActivity(), Activity_Ecnomia.class);
                 startActivity(Bmeters);
             }
        });

        artes=(Button)view.findViewById(R.id.button_Artes);
        artes.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 Intent Bmeters = new Intent( getActivity() ,Activity_artes.class);
                 startActivity(Bmeters);
             }
        });

        return view;
    }
}